# Load required packages
# R version 4.3.1
library(survival) # version 3.5.7
library(ggplot2) # version 3.4.3
library(rms) # version 6.7.0

# Load pbc data from survival package
data(pbc, package = "survival")

# set datadist (rms function)
dd <- datadist(pbc)
options(datadist = "dd")

# Create survival object
s <- with(pbc, Surv(time, status == 2))
# Fit Cox Proportional Hazards Model
mod <- cph(
    s ~ trt + rcs(age, 4) + rcs(bili, 4),
    x = TRUE, y = TRUE,
    data = pbc
)

# Plot Hazard Ratio plot with reference value (as per default) set to median of
# variable of interest (here: age), conditioned on covariables in the model.
ggplot(Predict(mod, age, fun = exp, ref.zero = TRUE)) +
    theme_bw(base_size = 16) +
    geom_hline(yintercept = 1, linetype = "dashed") +
    scale_y_continuous("Hazard ratio") +
    scale_x_continuous("Age in years")
